A PHP string truncation library with multibyte support.

## Usage

```php
StringTruncate::truncate('abcdefghijklmnopqrstuvwxyz', 10);
```

## Installation
Include the following in your composer.json file:

```json
"require": {
    "bknasmueller/php-truncate": "~0.1.0"
}
```