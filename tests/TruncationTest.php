<?php

require_once __DIR__ . '/../vendor/autoload.php';

use StringTruncate\StringTruncate;

class TruncationTest extends PHPUnit_Framework_TestCase
{
    /**
     * Asserts that the string is actually truncated.
     *
     */
    public function testTruncation()
    {
        $this->assertEquals('abc...', StringTruncate::truncate('abcdefghijklmnopqrstuvwxyz', 6));
        $this->assertEquals('abcdefg...', StringTruncate::truncate('abcdefghijklmnopqrstuvwxyz', 10));
    }
}
