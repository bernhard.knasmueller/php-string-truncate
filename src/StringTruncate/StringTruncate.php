<?php

namespace StringTruncate;

class StringTruncate
{
    /**
     * 
     */
    public static function truncate(string $string, int $maxLength, ?string $trailing = '...')
    {
        if ($trailing === null) {
            $trailing = '';
        }

        if ($maxLength === 0 && mb_strlen($trailing) === 0) {
            return '';
        }

        if (mb_strlen($trailing) >= $maxLength) {
            throw new \RuntimeException('Max length cannot be smaller than number of trailing characters');
        }

        return mb_substr($string, 0, $maxLength - mb_strlen($trailing)) . $trailing;
    }
}
